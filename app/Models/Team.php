<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;

class Team extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];


    /**
     * Route key 
     * 
     * @return string 
     */
    public function getRouteKeyName()
    {
    	return 'name';
    }


    /**
     * One to Many relation (inverse)
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
	public function group()
	{
		return $this->belongsTo('App\Models\Group');
	}


    /**
     * Every team can play with many related teams.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function secondTeam()
    {
        return $this->belongsToMany('App\Models\Team', 'matches', 'team_one_id', 'team_two_id')
                    ->withPivot('id', 'score_one', 'score_two');
    }


    /**
     * Generate matches from teams list
     * 
     * @param  array  $teamsArray
     * @return array $matches
     */
    public function generateMatches(array $teamsArray)
    {

        $teams = $this->getTeamCollection($teamsArray);

        while($teams->count() > 1) {

         $firstTeam = $teams->shift();

             foreach ($teams as $secondTeam) {

                 $firstTeam->secondTeam()->attach($secondTeam);
             }
        }  

        $teams = $this->getTeamCollection($teamsArray);

        $matches = $this->getMatches($teams);

        return $matches;
    }


    /**
     * Get Matches
     * 
     * @param  Collection $teams
     * @return array $matches
     */
    public function getMatches(Collection $teams)
    {
        $matches = [];

        foreach ($teams as $firstTeam) {

           foreach ($firstTeam->secondTeam as $secondTeam) {

               $matches[] = [
                    'first' => [
                        'team' =>  $firstTeam,
                        'score' =>  $secondTeam->pivot->score_one,
                    ], 
                    'second' => [
                        'team' =>  $secondTeam,
                        'score' =>  $secondTeam->pivot->score_two,
                    ],  
                    'id' => $secondTeam->pivot->id,
                ];
           }
       }

       return $matches;
    }

    /**
     * Get collection of teams (from App\Modes\Team)
     * 
     * @param  array  $teamsArray
     * @return Collection $teams
     */
    protected function getTeamCollection(array $teamsArray)
    {
        $ids = collect($teamsArray)->pluck('id');

        $teams = $this->query()->whereIn('id', $ids)->get();

        return $teams;
    }

}
