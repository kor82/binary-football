<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Exception;

class Group extends Model
{
	/**
	 * Available group names
	 * 
	 */
	const NAMES = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];


    /**
     * One to Many relation
     * 
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
	public function teams()
	{
		return $this->hasMany('App\Models\Team');
	}


	/**
	 * Create new group
	 * 
	 * @return App\Models\Group $group
	 */
	public function createNewGroup()
	{
		$lastGroupName = self::NAMES[0];

		if (self::count() > 0) {

	        $lastGroupName = $this->query()
	                              ->whereId(self::max('id'))
	                              ->firstOrFail()
	                              ->name;			
		} 

    	$newGroupIndex = $this->getNewGroupIndex($lastGroupName);

        $newGroupName = self::NAMES[$newGroupIndex];

        $newGroup = $this->create([
        	'name' => $newGroupName
        ]);

        return $newGroup;
	}


	/**
	 * Get index to define next group name
	 * 
	 * @param  string $lastSavedName
	 * @return integer $newIndex in const NAMES
	 */
	protected function getNewGroupIndex(string $lastSavedName)
	{
		$countOfGroups = self::count();
		if (!$countOfGroups) {
			return $newIndex = 0;
		}

		if ($countOfGroups == count(self::NAMES)) {
			throw new Exception('all groups are created');
		} 

		$lastAvailableName = array_values(array_slice(self::NAMES, -1))[0];

		if ($lastSavedName == $lastAvailableName) {

			$existingNames = self::all()->pluck('name')->toArray();

			$newName = array_diff(self::NAMES, $existingNames)[0];

			$newIndex = array_search($newName, self::NAMES);

		} else {

			$newIndex = array_search($lastSavedName, self::NAMES) + 1;
		}

		return $newIndex;
	}

}