<?php

namespace App\Http\Controllers;

use App\Models\Team;
use App\Models\Group;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Collection;

class TeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Team $team)
    {
        $queryString = $request->getQueryString();
        
        $groupName = explode('=', $queryString)[1];

        $teams = Group::whereName($groupName)
                        ->firstOrFail()
                        ->teams()
                        ->get();

        $matches = $team->getMatches($teams);

        return response()->json(compact(['teams', 'matches']));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'team' => ['required', 'string', 'max:50', 'unique:teams,name'],
            'group' => ['required', 'string', 'max:1', 'exists:groups,name'],
        ]);

        $group = Group::whereName($request->group)->firstOrFail();

        $team = Team::create(['name' => $request->team]);

        $team->group()->associate($group)->save();

        return response()->json($team);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $team = Team::findOrFail($id);

        $team->secondTeam()->sync([]);

        $result = Team::destroy($id);

        return response()->json($result, 202);
    }


    /**
     * Generate matches
     * 
     * @param  Request $request 
     * @param  Team   $team
     * @return array $matches
     */
    public function generateMatches(Request $request, Team $team)
    {
        $request->validate([
            'teams' => ['required', 'array', 'min:2'],
        ]);

        $mathes = $team->generateMatches($request->teams);

        return response()->json($mathes);   
    }


    /**
     * Update matches score
     * 
     * @param  Request $request
     * @param  integer $id 
     * @return string json
     */
    public function updateMatches(Request $request, $id)
    {
        $request->validate([
            'matchId' => ['required', 'integer', 'exists:matches,id'],
            'number'  => ['required', 'string', 'in:one,two'],
            'value'   => ['nullable', 'integer'],
        ]);

        $field = 'score_'.$request->number;

        DB::table('matches')
            ->whereId($request->matchId)
            ->update([$field => $request->value]);

        return response()->json('updated', 202);
    }

}
