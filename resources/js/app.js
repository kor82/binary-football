/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

import Teams from './views/Teams'
import Groups from './views/Groups'

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'group',
            component: Groups
        },
        {
            path: '/group/:name',
            name: 'team',
            component: Teams,
        },       
    ],
});

Vue.mixin({
  methods: {
        renderErrors(responseErrors) {
            let errors = responseErrors;
            let result = [];
            for (let error in errors) {
                errors[error].forEach(item => result.push(item))
            }
            this.errors = result;
        },
        goBack() {
          window.history.length > 1
            ? this.$router.go(-1)
            : this.$router.push('/')
        },        
  }
});

const app = new Vue({
    el: '#app',
    components: { Groups, Teams },
    router,
});

