<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::delete('/{id}', 'GroupController@destroy')
     ->where('id', '[0-9]+');

Route::resource('/', 'GroupController')
     ->only(['index', 'store']);

Route::post('group/matches', 'TeamController@generateMatches');

Route::put('group/matches/{id}', 'TeamController@updateMatches')
     ->where('id', '[0-9]+');

Route::resource('group', 'TeamController')
	 ->only(['index', 'store', 'destroy']);